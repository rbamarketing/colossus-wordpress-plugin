<?php


include_once('aquarium_LifeCycle.php');

class aquarium_Plugin extends aquarium_LifeCycle {

    /**
     * See: http://plugin.michael-simpson.com/?page_id=31
     * @return array of option meta data.
     */
    public function getOptionMetaData() {
        //  http://plugin.michael-simpson.com/?page_id=31
        return array(
            //'_version' => array('Installed Version'), // Leave this one commented-out. Uncomment to test upgrades.
            'DoCommandURL' => array(__('URL of docommand.php', 'my-awesome-plugin')),
            'CCC_command' => array(__('Command Line of colossuscontrol.py', 'my-awesome-plugin')),
            'sqldatabase' => array(__('Colossus MySql Database','my-awesome-plugin')),
            'sqlserver' => array(__('Colossus MySql Server','my-awesome-plugin')),
            'sqluser' => array(__('Colossus MySql User','my-awesome-plugin')),
            'sqlpass' => array(__('Colossus MySql Password','my-awesome-plugin')),
            'CanDoSomething' => array(__('Which user role can do something', 'my-awesome-plugin'),
                                        'Administrator', 'Editor', 'Author', 'Contributor', 'Subscriber', 'Anyone')
        );
    }

//    protected function getOptionValueI18nString($optionValue) {
//        $i18nValue = parent::getOptionValueI18nString($optionValue);
//        return $i18nValue;
//    }

    protected function initOptions() {
        $options = $this->getOptionMetaData();
        if (!empty($options)) {
            foreach ($options as $key => $arr) {
                if (is_array($arr) && count($arr > 1)) {
                    $this->addOption($key, $arr[1]);
                }
            }
        }
    }

    public function getPluginDisplayName() {
        return 'Colossus Aquarium';
    }

    protected function getMainPluginFileName() {
        return 'colossus-aquarium.php';
    }

    /**
     * See: http://plugin.michael-simpson.com/?page_id=101
     * Called by install() to create any database tables if needed.
     * Best Practice:
     * (1) Prefix all table names with $wpdb->prefix
     * (2) make table names lower case only
     * @return void
     */
    protected function installDatabaseTables() {
        //        global $wpdb;
        //        $tableName = $this->prefixTableName('mytable');
        //        $wpdb->query("CREATE TABLE IF NOT EXISTS `$tableName` (
        //            `id` INTEGER NOT NULL");
    }

    /**
     * See: http://plugin.michael-simpson.com/?page_id=101
     * Drop plugin-created tables on uninstall.
     * @return void
     */
    protected function unInstallDatabaseTables() {
        //        global $wpdb;
        //        $tableName = $this->prefixTableName('mytable');
        //        $wpdb->query("DROP TABLE IF EXISTS `$tableName`");
    }


    /**
     * Perform actions when upgrading from version X to version Y
     * See: http://plugin.michael-simpson.com/?page_id=35
     * @return void
     */
    public function upgrade() {
    }
    
    public function func_mysql_status() {
    
        $colossusdb =  mysql_connect(get_option('aquarium_Plugin_sqlserver'),get_option('aquarium_Plugin_sqluser'),get_option('aquarium_Plugin_sqlpass'));
        mysql_select_db(get_option('aquarium_Plugin_sqldatabase'), $colossusdb);
            

        $sql = "SELECT TABLE_NAME,SUM(TABLE_ROWS)   FROM INFORMATION_SCHEMA.TABLES   WHERE TABLE_SCHEMA = 'colossus'   GROUP BY TABLE_NAME;";
        $result = mysql_query($sql,$colossusdb) or die(mysql_error());
        $output= '<table>';
        $output .= '<tr><th style="color:white;">mySQL Table</th><th style="color:white;"># Records</th></tr>';
        while ($row = mysql_fetch_array($result))
            {
                $output .=  '<tr><td align="left" style="color:white">'.  $row[0].'</td> ';
                $output .=  '<td align="right" style="color:white">'.$row[1].'</td></tr>';

            }
        $output .=  "</table>";
        mysql_free_result($result);
        mysql_close($colossusdb);
        return $output;
    }
    
    
    
    public function func_relay_status()
        {
            $syscmd = get_option('aquarium_Plugin_CCC_command');
            $syscmd .= " webstatus";
            exec($syscmd, $result);
            $chunks = array_chunk(preg_split('/(-|,)/', $result[0]), 2);
            $result = array_combine(array_column($chunks, 0), array_column($chunks, 1));
            ksort($result);
            //print_r($result);
            $output .=  '<div class="w3-row">';
            foreach ($result as $key => $val) 
                {
                    $output .= self::printpowerbutton($key,$val);
                }
            $output .=  "</div>";
            return $output;
        }
        
   public function createcommand($key,$value)
        {
            $syscommand = get_option('aquarium_Plugin_DoCommandURL');
            $syscommand .= "?cmd=".$key;
            return $syscommand;
        }

    public function printpowerbutton($key,$value)
        {
            $output =  '<div class="w3-col s4">';
            $output .=  '<a href="'.self::createcommand($key,$value).'"class="w3-button w3-center w3-block w3-hover-blue ';
            if ($value == 1)
                {
                    $output .=  ' w3-indigo w3-text-yellow "> ';
                    $output .=  '<i class="fa  fa-cog fa-spin fa-lg w3-text-yellow "></i> ';
                }
            else 
                {
                    $output .=  ' w3-light-gray w3-text-black " >';
                    $output .=  '<i class="fa  fa-power-off fa-lg w3-text-black "></i> ';
                }
            $output .=  strtoupper($key);
            $output .=  '</a></div>';
            return $output;            
        }
    
   public function setcolor($temp)
        {   
            // https://bitbucket.org/rbamarketing/colossus-wordpress-plugin/issues/1
            
            if ($temp <= 80 && $temp >= 77)
                {
                    $output .=  'w3-green';
                }
            if ( $temp >= 81)
                {
                    $output .=  'w3-red';
                }
            if ( $temp <= 75)
                {
                    $output .=  'w3-blue';
                }
            return $output;
       }
   
   public function display_temp($temp)
        {
            // https://bitbucket.org/rbamarketing/colossus-wordpress-plugin/issues/1        
        
            if ($temp <= 80 && $temp >= 77)
                {
                    $output .=  '<i class="fa fa-thermometer-half"></i> '.$temp;
                }
            elseif ($temp >= 81)
                {
                    $output .=  '<i class="fa fa-thermometer-full"></i> '.$temp;
                }
            elseif ( $temp <= 75)
                {
                    $output .=  '<i class="fa fa-thermometer-empty"></i> '.$temp;
                }
            return $output;
        }
   public function func_display_tank_temps()
        {
            // https://bitbucket.org/rbamarketing/colossus-wordpress-plugin/issues/1
        
            $colossusdb =  mysql_connect(get_option('aquarium_Plugin_sqlserver'),get_option('aquarium_Plugin_sqluser'),get_option('aquarium_Plugin_sqlpass'));
            mysql_select_db(get_option('aquarium_Plugin_sqldatabase'), $colossusdb);
            
            $sql = "SELECT * FROM `temperatures` WHERE `sensor` = \"colossus-Hood\" order by `date` desc limit 1";
            $result = mysql_query($sql,$colossusdb) or die(mysql_error());
            $output .=  '<div class="w3-row">';
            while ($row = mysql_fetch_assoc($result))
                {
                    $temp = ceil($row['temperature']);
                    $output .=  '<div class=" w3-col w3-green s4">';
                    $output .=  '<h1 class="temperature w3-center w3-text-white" />';
                    $output .= '<i class="fa fa-home"></i> '.$temp;
                    $output .= "</h1>";
                    $output .= "</div>";
                }
            mysql_free_result($result);
            $sql = "SELECT * FROM `temperatures` WHERE `sensor` = \"colossus-Tank-Right\" order by `date` desc limit 1";
            $result = mysql_query($sql,$colossusdb) or die(mysql_error());
            while ($row = mysql_fetch_assoc($result))
                {
                    $temp = ceil($row['temperature']);
                    $output .= '<div class="w3-col ';
                    $output .= self::setcolor($temp);
                    $output .= ' s4">';
                    $output .= '<h1 class="temperature w3-center w3-text-white" />';
                    $output .= self::display_temp($temp);
                    $output .= "</h1>";
                    $output .= "</div>";
                }
            mysql_free_result($result);
            $sql = "SELECT * FROM `temperatures` WHERE `sensor` = \"colossus-Tank-Left\" order by `date` desc limit 1";
            $result = mysql_query($sql,$colossusdb) or die(mysql_error());
            while ($row = mysql_fetch_assoc($result))
                {
                    $temp = ceil($row['temperature']);
                    $output .= '<div class="w3-col ';
                    $output .= self::setcolor($temp);
                    $output .= ' s4">';
                    $output .= '<h1 class="temperature w3-center w3-text-white" />';
                    $output .= self::display_temp($temp);
                    $output .= "</h1>";
                    $output .= "</div>";
                }
           $output .= '</div>'; 
           mysql_close($colossusdb);
           mysql_free_result($result);
           return $output;
        } // End func_display_tank_temps
    
    public function func_display_last_event()
        {
            $colossusdb =  mysql_connect(get_option('aquarium_Plugin_sqlserver'),get_option('aquarium_Plugin_sqluser'),get_option('aquarium_Plugin_sqlpass'));
            mysql_select_db(get_option('aquarium_Plugin_sqldatabase'), $colossusdb);
            $sql = "SELECT * FROM `events` ORDER BY `eventid` DESC limit 1 ";
            $result = mysql_query($sql,$colossusdb) or die(mysql_error());
            $output .=  '<div class="w3-row">';
            while ($row = mysql_fetch_assoc($result))
                {
                     $output .=  '<p style="color:white;">Last Event: ';
                     $output .=  $row[date]."<br /> ".$row[sensor]." - ".$row[eventdata]."</p>";
                }
            $output .= '</div>';
            mysql_close($colossusdb);
            mysql_free_result($result);
            return $output;
        }
    public function func_display_last_ph()
        {
            $colossusdb =  mysql_connect(get_option('aquarium_Plugin_sqlserver'),get_option('aquarium_Plugin_sqluser'),get_option('aquarium_Plugin_sqlpass'));
            mysql_select_db(get_option('aquarium_Plugin_sqldatabase'), $colossusdb);
            $sql = "SELECT `date`, `phlevel` FROM `waterph` WHERE 1 order by `phid` desc limit 1";
            $result = mysql_query($sql,$colossusdb) or die(mysql_error());
            $output .=  '<div  class="w3-row w3-text-white">';
            while ($row = mysql_fetch_assoc($result))
                {
                    $mydate = self::display_date($row[date]);
                    $output .=  '<p style="color:white;">Last PH Reading: <br />';
                    $output .=  $mydate.": ".$row[phlevel]."</p>";
                }
            $output .=  '</div>';
            mysql_close($colossusdb);
            mysql_free_result($result);
            return $output;
        }
        
    public function display_date($date)
        {
            $tmparray = preg_split("/ /",$date);
            $newdate = $tmparray[0];
            //print_r($tmparray);
            return $newdate;
        }
    
    public function next_water_change($date)
        {
            $newdate = date('Y-m-d', strtotime($date.' + 14 days'));
            return $newdate;
        }
        
        
        
    public function func_display_last_waterchange()
        {
            $colossusdb =  mysql_connect(get_option('aquarium_Plugin_sqlserver'),get_option('aquarium_Plugin_sqluser'),get_option('aquarium_Plugin_sqlpass'));
            mysql_select_db(get_option('aquarium_Plugin_sqldatabase'), $colossusdb);
            $sql = "SELECT sum(`gallons`) FROM `waterchanges` WHERE 1";
            $result = mysql_query($sql, $colossusdb) or die(mysql_error("here"));
            while ($row = mysql_fetch_array($result))
                {
                    $output .=  '<p>Total Water Usage: '.$row[0].' Gallons</p>';
                }
            $sql = "SELECT `date`, `gallons` FROM `waterchanges` WHERE 1 order by `wcid` desc limit 1";
            $result = mysql_query($sql) or die(mysql_error());
            $output .=  '<div style="color:white;">';
            while ($row = mysql_fetch_assoc($result))
                {
                    $mydate = self::display_date($row[date]);
                    $output .=  '<p style="color:white;">Last Water Change: <br/>';
                    $output .=  $mydate.": ".$row[gallons]." gallons <br />";
                    $output .= 'Next Water Change:<br />';
                    $output .= self::next_water_change($mydate)."</p>";
                }
            $output .=  '</div>';
            mysql_close($colossusdb);
            mysql_free_result($result);
            return $output;
        }
        
    public function process_date($date)
        {
            $tmpdate = preg_split("/ /",$date);
            $partdate = preg_split("/-/",$tmpdate[0]);
            $return = "new Date(".intval($partdate[0]).",".intval($partdate[1]-1).",".intval($partdate[2]).")";
            return $return;
        }
        
    public function func_graph_temps()
        {
        
            $output = '<script type="text/javascript">';
            $output .= "google.charts.load('current', {packages: ['corechart', 'line']});google.charts.setOnLoadCallback(drawCurveTypes);";
            $output .= 'function drawCurveTypes() {';
            $output .= 'var data = new google.visualization.DataTable();';
            $output .= "data.addColumn('date', 'Dogs');";
            $output .= "data.addColumn('number', 'Temperature');";
            $output .= '      data.addRows([';
            $colossusdb =  mysql_connect(get_option('aquarium_Plugin_sqlserver'),get_option('aquarium_Plugin_sqluser'),get_option('aquarium_Plugin_sqlpass'));
            mysql_select_db(get_option('aquarium_Plugin_sqldatabase'), $colossusdb);
            $sql = "select * from temperatures where date like '%-%-% 12:00:%' and sensor = 'Colossus-Tank-Right' order by date desc limit 90";
            $result = mysql_query($sql,$colossusdb) or die(mysql_error());
            while ($row = mysql_fetch_assoc($result))
                {
                    $output .=  "[";
                    $output .=  self::process_date($row['date']);
                    $output .=  ",".$row[temperature]."],";
                }
            $output .= '      ]);';
            $output .= '      var options = {';
            $output .= '        hAxis: {';
            $output .= "          title: 'Date'";
            $output .= '        },';
            $output .= '        vAxis: {';
            $output .= "          title: 'Temperature'";
            $output .= '        },';
            $output .= '        series: {';
            $output .= "          1: {curveType: 'function'}";
            $output .= '        }';
            $output .= '      };';
            $output .= "      var chart = new google.visualization.LineChart(document.getElementById('chart1_div'));";
            $output .= '      chart.draw(data, options);';
            $output .= '}';
            $output .= '</script>';
            $output .= '<div id="chart1_div"></div>';
            mysql_free_result($result);
            mysql_close($colossusdb);
            return($output);
        }
        
    public function func_graph_water_changes() 
        {
            $output = '<script type="text/javascript">';
            $output .= "google.charts.load('current', {packages: ['corechart', 'line']});google.charts.setOnLoadCallback(drawCurveTypes);";
            $output .= 'function drawCurveTypes() {';
            $output .= 'var data = new google.visualization.DataTable();';
            $output .= "data.addColumn('date', 'Dogs');";
            $output .= "data.addColumn('number', 'Gallons');";
            $output .= '      data.addRows([';
            $colossusdb =  mysql_connect(get_option('aquarium_Plugin_sqlserver'),get_option('aquarium_Plugin_sqluser'),get_option('aquarium_Plugin_sqlpass'));
            mysql_select_db(get_option('aquarium_Plugin_sqldatabase'), $colossusdb);
            $result = mysql_query($sql,$colossusdb) or die(mysql_error());
            while ($row = mysql_fetch_assoc($result))
                {
                    $output .=  "[";
                    $output .=  self::process_date($row['date']);
                    $output .=  ",".$row[gallons]."],";
                }
            $output .= '      ]);';
            $output .= '      var options = {';
            $output .= '        hAxis: {';
            $output .= "          title: 'Date'";
            $output .= '        },';
            $output .= '        vAxis: {';
            $output .= "          title: 'Gallons'";
            $output .= '        },';
            $output .= '        series: {';
            $output .= "          1: {curveType: 'function'}";
            $output .= '        }';
            $output .= '      };';
            $output .= "      var chart = new google.visualization.LineChart(document.getElementById('chart2_div'));";
            $output .= '      chart.draw(data, options);';
            $output .= '}';
            $output .= '</script>';
            $output .= '<div id="chart2_div"></div>';
            mysql_free_result($result);
            mysql_close($colossusdb);
            return($output);
        }
        
     public function func_graph_ph() 
        {
            $output = '<script type="text/javascript">';
            $output .= "google.charts.load('current', {packages: ['corechart', 'line']});google.charts.setOnLoadCallback(drawCurveTypes);";
            $output .= 'function drawCurveTypes() {';
            $output .= 'var data = new google.visualization.DataTable();';
            $output .= "data.addColumn('date', 'Dogs');";
            $output .= "data.addColumn('number', 'pH');";
            $output .= '      data.addRows([';
            $colossusdb =  mysql_connect(get_option('aquarium_Plugin_sqlserver'),get_option('aquarium_Plugin_sqluser'),get_option('aquarium_Plugin_sqlpass'));
            mysql_select_db(get_option('aquarium_Plugin_sqldatabase'), $colossusdb);
            $sql =  "select * from waterph where 1 order by date asc";
            $result = mysql_query($sql,$colossusdb) or die(mysql_error());
            while ($row = mysql_fetch_assoc($result))
                {
                    $output .=  "[";
                    $output .=  self::process_date($row['date']);
                    $output .=  ",".$row[phlevel]."],";
                }
            $output .= '      ]);';
            $output .= '      var options = {';
            $output .= '        hAxis: {';
            $output .= "          title: 'Date'";
            $output .= '        },';
            $output .= '        vAxis: {';
            $output .= "          title: 'pH Level'";
            $output .= '        },';
            $output .= '        series: {';
            $output .= "          1: {curveType: 'function'}";
            $output .= '        }';
            $output .= '      };';
            $output .= "      var chart = new google.visualization.LineChart(document.getElementById('chart3_div'));";
            $output .= '      chart.draw(data, options);';
            $output .= '}';
            $output .= '</script>';
            $output .= '<div id="chart3_div"></div>';
            mysql_free_result($result);
            mysql_close($colossusdb);
            return($output);
        } 
        
    public function func_showcamera($atts)
        {
            $a = shortcode_atts( array('cameraurl' => 'Empty','title' => 'No Title'),$atts);
            $output = '<div class="w3-card-4"><header class="w3-container w3-blue"><h1 style="color: white; text-align: center;">';
            $output .= $a['title'];
            $output .= '</h1></header><div class="w3-container"><img style="width: 100%" src = "';
            $output .= $a['cameraurl'];
            $output .= '" /><a style="color: white; text-align: center;" href="';
            $output .= $a['cameraurl'];
            $output .= '">View Full Screen</a></div></div>';
            return $output;
        }
 
    public function addActionsAndFilters() {

        // Add options administration page
        // http://plugin.michael-simpson.com/?page_id=47

        add_action('admin_menu', array(&$this, 'addSettingsSubMenuPage'));
        add_shortcode('colossus-mysql-status', array($this, 'func_mysql_status'));
        add_shortcode('colossus-relay-status', array($this, 'func_relay_status'));
        add_shortcode('colossus-temperature-status', array($this, 'func_display_tank_temps'));
        add_shortcode('colossus-display-last-event',array($this,'func_display_last_event'));
        add_shortcode('colossus-display-last-ph',array($this,'func_display_last_ph'));
        add_shortcode('colossus-display-last-waterchange',array($this,'func_display_last_waterchange'));
        add_shortcode('colossus-graph-water-change',array($this,'func_graph_water_changes'));
        add_shortcode('colossus-graph-temperature',array($this,'func_graph_temps'));
        add_shortcode('colossus-graph-ph',array($this,'func_graph_ph'));
        add_shortcode('colossus-show-camera',array($this,'func_showcamera'));
        
        
        // Example adding a script & style just for the options administration page
        // http://plugin.michael-simpson.com/?page_id=47
        //        if (strpos($_SERVER['REQUEST_URI'], $this->getSettingsSlug()) !== false) {
        //            wp_enqueue_script('my-script', plugins_url('/js/my-script.js', __FILE__));
        //            wp_enqueue_style('my-style', plugins_url('/css/my-style.css', __FILE__));
        //        }

        wp_register_script( 'Google_chart', 'https://www.gstatic.com/charts/loader.js' );
        wp_enqueue_script('Google_chart');
        
        
        // Add Actions & Filters
        // http://plugin.michael-simpson.com/?page_id=37

        
        wp_enqueue_style('my-w3','https://www.w3schools.com/w3css/4/w3.css');
        wp_enqueue_style('my-fontawesome',plugins_url('/fontawesome/css/font-awesome.min.css', __FILE__));
       
       
        // Adding scripts & styles to all pages
        // Examples:
        //        wp_enqueue_script('jquery');
        //        wp_enqueue_style('my-style', plugins_url('/css/my-style.css', __FILE__));
        //        wp_enqueue_script('my-script', plugins_url('/js/my-script.js', __FILE__));


        // Register short codes
        // http://plugin.michael-simpson.com/?page_id=39


        // Register AJAX hooks
        // http://plugin.michael-simpson.com/?page_id=41

    }

}
